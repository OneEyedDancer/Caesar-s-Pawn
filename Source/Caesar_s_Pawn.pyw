from tkinter import *
from tkinter import messagebox
from webbrowser import open_new
from pyperclip import copy, paste

window = Tk()
window.resizable(width=False, height=False)
window['bg'] = '#8B0000'
window.title("Caesar's Pawn v1.3")
window.geometry('450x320+700+350')
window.iconbitmap('Data/icon.ico')

# майнер

entry_text = StringVar()
entry_key = StringVar()
encrypt_state = BooleanVar()
eng_alphabet = 'abcdefghijklmnopqrstuvwxyz'
rus_alphabet = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя'

Label(window,
      text="Caesar's Pawn",
      font='AdobeFangsongStd-Regular 30',
      fg='#FFD700',
      bg='#8B0000'
      ).place(x=0, y=0)

Label(window,
      text='Enter the key',
      font='AdobeFangsongStd-Regular',
      fg='#FFD700',
      bg='#8B0000'
      ).place(x=0, y=60)

Entry(window,
      width=28,
      textvariable=entry_key,
      bg='#8B0000',
      fg='#FFD700',
      font='AdobeFangsongStd-Regular'
      ).place(x=150, y=65)

Label(window,
      text='Enter the message',
      fg='#FFD700',
      bg='#8B0000',
      font='AdobeFangsongStd-Regular'
      ).place(x=0, y=90)

Entry(window,
      width=28,
      textvariable=entry_text,
      bg='#8B0000',
      fg='#FFD700',
      font=('Times New Roman', 15)
      ).place(x=150, y=95)


def encrypt():
    key = 0
    try:
        key = abs(int(entry_key.get()))
    except ValueError:
        messagebox.showerror('Ошибочка...', 'Ключ должен быть числом!')
    text = (entry_text.get()).split()  # делит исходный текст на слова
    result = ''
    if encrypt_state.get():  # проверяет состояние нажатой галочки рядом с Encrypt
        key = -key
    for word in range(len(text)):
        for char in range(len(text[word])):
            if text[word][char].isalpha():  # проверка на букву
                if text[word][char].lower() in eng_alphabet:  # выбор алфавита
                    alphabet = eng_alphabet
                else:
                    alphabet = rus_alphabet
                position = alphabet.find(text[word][char].lower())  # нахождение буквы в алфавите
                if position + abs(key) >= len(alphabet):  # если номер буквы с ключом выходит за алфавит
                    position = (key + position) % len(alphabet)
                else:
                    position += key
                result += alphabet[position].upper() if text[word][char].isupper() else alphabet[position]  # сверяет размер буквы
            else:  # если не буква, то вставляет без изменений
                result += text[word][char]
        result += ' '  # пробел после слова
    text_box.delete('1.0', END)
    text_box.insert('1.0', result)
    window.title('Ave Caesar! Morituri te salutant')


def decryptor_window():
    dec_win = Toplevel()
    dec_win.resizable(width=False, height=False)
    dec_win.geometry('600x750')
    dec_win.title('Deciphering the Caesar Cipher')
    dec_win.iconbitmap('Data/Icon.ico')
    dec_win['bg'] = 'black'

    frame_cnt = 5
    frames = [PhotoImage(file='Data/background.gif', format='gif -index %i' % i) for i in range(frame_cnt)]

    def update(ind):  # анимация для гиф файла
        frame = frames[ind]
        ind += 1
        if ind == frame_cnt:
            ind = 0
        gif.configure(image=frame)
        dec_win.after(70, update, ind)

    gif = Label(dec_win, borderwidth=0)
    gif.place(x=-150, y=-300)
    dec_win.after(0, update, 0)

    def decryptor():
        encrypted_text = (text_box_input.get(1.0, END)).split()
        text_box_output.delete(1.0, END)
        text_with_key = ''
        for keys in range(1, 32):  # перебор всех ключей
            key = -keys
            text = ''
            for word in range(len(encrypted_text)):
                for char in range(len(encrypted_text[word])):
                    if encrypted_text[word][char].isalpha():  # проверка на букву
                        if encrypted_text[word][char].lower() in eng_alphabet:  # выбор алфавита
                            alphabet = eng_alphabet
                        else:
                            alphabet = rus_alphabet
                        position = alphabet.find(encrypted_text[word][char].lower())  # нахождение буквы в алфавите
                        if position + abs(key) >= len(alphabet):  # если номер буквы с ключом выходит за алфавит
                            position = (key + position) % len(alphabet)
                        else:
                            position += key
                        text += alphabet[position].upper() if encrypted_text[word][char].isupper() else alphabet[
                            position]  # сверяет размер буквы
                    else:  # если не буква, то вставляет без изменений
                        text += encrypted_text[word][char]
                text += ' '  # пробел после слова
            text_with_key += f' \n{text} \n{"="*10}(key: {key}){"="*11}\n'
        text_box_output.insert(1.0, text_with_key)  # вывод всех вариантов

    Label(dec_win,
          text="Caesar's Pawn",
          font='AdobeFangsongStd-Regular 30',
          fg='#32CD32',
          bg='black'
          ).pack(side=TOP)

    Label(dec_win,
          text='Decryptor',
          font='AdobeFangsongStd-Regular 15',
          fg='#32CD32',
          bg='black'
          ).place(x=250, y=45)

    Label(dec_win,
          text='Encrypted text                             Decrypted text',
          font='AdobeFangsongStd-Regular 15',
          fg='#32CD32',
          bg='black'
          ).place(x=80, y=120)

    def paste_text():
        text_box_input.delete(1.0, END)
        text_box_input.insert(1.0, paste())

    Button(dec_win,
           text='Paste',
           command=paste_text,
           fg='#32CD32',
           bg='black',
           font='AdobeFangsongStd-Regular',
           activebackground='#32CD32',
           activeforeground='black',
           relief='ridge',
           cursor="hand2"
           ).place(x=17, y=118)

    Button(dec_win,
           text='-->',
           command=decryptor,
           fg='#32CD32',
           bg='black',
           font='Arial 13',
           activebackground='#32CD32',
           activeforeground='black',
           relief='ridge',
           cursor="hand2"
           ).place(x=283, y=120)

    text_box_input = Text(dec_win,
                          width=32,
                          height=31,
                          fg='#32CD32',
                          bg='black',
                          relief='sunken',
                          font=('Times New Roman', 13))
    text_box_input.pack(side=LEFT, anchor=S)

    text_box_output = Text(dec_win,
                           width=32,
                           height=31,
                           fg='#32CD32',
                           bg='black',
                           relief='sunken',
                           font=('Times New Roman', 13))
    text_box_output.pack(side=RIGHT, anchor=S)

    dec_win.mainloop()


Button(window,
       text='Decryptor',
       command=decryptor_window,
       fg='#FFD700',
       bg='#8B0000',
       font='AdobeFangsongStd-Regular',
       activebackground='#8B0000',
       activeforeground='#FFD700',
       cursor="hand2"
       ).place(x=350, y=135)

Button(window,
       text='Confirm',
       command=encrypt,
       fg='#FFD700',
       bg='#8B0000',
       font='AdobeFangsongStd-Regular',
       activebackground='#8B0000',
       activeforeground='#FFD700',
       cursor="hand2"
       ).place(x=5, y=135)

Button(window,
       text='Copy',
       command=lambda: copy(text_box.get(1.0, END)),
       fg='#FFD700',
       bg='#8B0000',
       font='AdobeFangsongStd-Regular',
       activebackground='#8B0000',
       activeforeground='#FFD700',
       cursor="hand2",
       relief='groove'
       ).place(x=-1, y=290)

Checkbutton(window,
            text='Encrypt',
            var=encrypt_state,
            fg='#FFD700',
            bg='#8B0000',
            font='AdobeFangsongStd-Regular',
            selectcolor='#8B0000',
            activebackground='#8B0000',
            activeforeground='#FFD700'
            ).place(x=85, y=135)

text_box = Text(
    width=50,
    height=6,
    fg='#FFD700',
    bg='#8B0000',
    font=('Times New Roman', 13))
text_box.place(x=0, y=180)


def open_author(event):
    open_new("https://www.youtube.com/watch?v=dQw4w9WgXcQ")  # (͡° ͜ʖ ͡°)


link = Label(window,
             text="By TakPlintus",
             fg="#FFD700",
             font='AdobeFangsongStd-Regular',
             cursor="hand2",
             bg='#8B0000')
link.place(x=348, y=292)
link.bind('<Button-1>', open_author)

window.mainloop()
